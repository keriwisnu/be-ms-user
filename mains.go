package main

import (
	"gitlab.com/keriwisnu/config"
	"gitlab.com/keriwisnu/controller"
	"gitlab.com/keriwisnu/middleware"
	repoUser "gitlab.com/keriwisnu/repository"
	serviceUser "gitlab.com/keriwisnu/service"
	"fmt"
	"log"
	"io"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/subosito/gotenv"
)

func init() {
	gotenv.Load()
}

func setupLogOutput() {
	f, _ := os.Create("gin.Log")
	gin.DefaultWriter = io.MultiWriter(f, os.Stdout)
}

func main() {
	db := config.Connect()
	defer db.Close()

	setupLogOutput()
	router := gin.New()
	router.Use(gin.Recovery(), middleware.Logger())

	userRepo := repoUser.CreateUserRepoImpl(db)

	userService := serviceUser.CreateUserServiceImpl(userRepo)
	controllers.CreateUserController(router, userService)

	fmt.Println("Starting web server at port : 9093")
	err := router.Run(":9093")
	if err != nil {
		log.Fatal()
	}
}
