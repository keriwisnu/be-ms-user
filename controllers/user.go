package controllers

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/keriwisnu/be-ms-user/model"
	"gitlab.com/keriwisnu/be-ms-user/user"
	"gitlab.com/keriwisnu/be-ms-user/utils"

	"github.com/gin-gonic/gin"
)

// UserController ...
type UserController struct {
	userService user.UserService
}

// CreateUserController ...
func CreateUserController(router *gin.Engine, userService user.UserService) {
	inDB := UserController{userService}
	// router := gin.New()
	// router.Use(gin.Recovery(), middleware.Logger())

	router.POST("/login", inDB.loginUser)
	router.POST("/user", inDB.createUser)

	// router.Use(middleware.TokenVerifyMiddleware())

	router.GET("/user", inDB.getAll)
	router.GET("/user/:id", inDB.getByID)
	router.PUT("/user/:id", inDB.updateData)
	router.DELETE("/user/:id", inDB.delete)

}

func (u *UserController) createUser(c *gin.Context) {
	var user model.User

	err := c.ShouldBindJSON(&user)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, "Oppss, something error")
		fmt.Printf("[UserHandler.InsertData] Error when decoder data from body with error : %v\n", err)
		return
	}

	dataHashed := utils.HashedPassword(c, user)

	data, err := u.userService.CreateUser(dataHashed)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, err.Error())
		fmt.Printf("[UserHandler.InsertData] Error when request data to usecase with error: %v\n", err)
		return
	}

	utils.SendEmail(data)
	utils.HandleSuccess(c, http.StatusOK, data)
}

func (u *UserController) getAll(c *gin.Context) {
	dataUser, err := u.userService.GetAll()
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, "Oppss, something error ")
		fmt.Printf("[UserHandler.getAll] Error when request data to usecase with error: %v\n", err)
		return
	}

	utils.HandleSuccess(c, http.StatusOK, dataUser)
}

func (u *UserController) getByID(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.HandleError(c, http.StatusBadRequest, "ID not valid !!")
		fmt.Printf("[UserHandler] Error when convert pathvar with error: %v\n", err)
	}

	data, err := u.userService.GetByID(id)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, "Opps, something error")
		fmt.Printf("[UserHandler.getByID]Error when request data with error : %v \n", err)
		return
	}

	utils.HandleSuccess(c, http.StatusOK, data)
}

func (u *UserController) updateData(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.HandleError(c, http.StatusBadRequest, "ID not valid !!")
		fmt.Printf("[UserHandler.updateData] Error when convert pathvar with error: %v\n", err)
	}

	var user model.User

	err = c.ShouldBindJSON(&user)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, "Oppss, something error")
		fmt.Printf("[UserHandler.InsertData] Error when decoder data from body with error : %v\n", err)
		return
	}

	dataHashed := utils.HashedPassword(c, user)

	data, err := u.userService.UpdateUser(id, dataHashed)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, err.Error())
		fmt.Printf("[UserHandler.update] Error when request data to usecase with error : %v", err)
		return
	}
	utils.HandleSuccess(c, http.StatusOK, data)
}

func (u *UserController) delete(c *gin.Context) {
	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.HandleError(c, http.StatusBadRequest, "ID not valid !!")
		fmt.Printf("[UserHandler.updateData] Error when convert pathvar with error: %v\n", err)
	}
	err = u.userService.DeleteUser(id)
	if err != nil {
		utils.HandleError(c, http.StatusNoContent, "Oppss, something error")
		fmt.Printf("[UserHandler.delete]Error when request data to usecase with error : %v\n", err)
		return
	}
	utils.HandleSuccess(c, http.StatusOK, "Delete data success ")
}

func (u *UserController) loginUser(c *gin.Context) {
	var data model.User
	var jwt model.Jwt

	err := c.ShouldBindJSON(&data)
	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, "Oppss, something error")
		fmt.Printf("[UserHandler.InsertData] Error when decoder data from body with error : %v\n", err)
		return
	}

	isValid := utils.ValidateLogin(c, &data)

	if !isValid {
		return
	}

	checkEmail := utils.CheckValidMail(data.Email)
	if !checkEmail {
		utils.HandleError(c, http.StatusBadRequest, "Email Address invalid")
		return
	}

	password := data.Password

	userData, err := u.userService.GetUserByEmail(data.Email)

	if err != nil {
		utils.HandleError(c, http.StatusInternalServerError, err.Error())
		return
	}

	isCorrect := utils.ComparePassword(userData.Password, []byte(password))

	if isCorrect {
		token, err := utils.GenerateToken(userData)
		if err != nil {
			utils.HandleError(c, http.StatusInternalServerError, "ooppss, something error")
			fmt.Printf("[UserHandler.GenerateToken]Error occured while generate token with error :%v\n", err)
		}
		jwt.Token = token
		utils.HandleSuccess(c, http.StatusOK, jwt)
	} else {
		utils.HandleError(c, http.StatusInternalServerError, "Invalid Password")
	}
}
