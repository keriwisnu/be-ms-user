module gitlab.com/keriwisnu/be-ms-user

go 1.13

require (
	github.com/asaskevich/govalidator v0.0.0-20200108200545-475eaeb16496 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.2
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/jinzhu/gorm v1.9.12
	github.com/subosito/gotenv v1.2.0
	golang.org/x/crypto v0.0.0-20200420201142-3c4aac89819a
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
