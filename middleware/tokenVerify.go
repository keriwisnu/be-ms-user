package middleware

import (
	"fmt"
	"net/http"
	"os"
	"strings"

	"gitlab.com/keriwisnu/be-ms-user/utils"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

// TokenVerifyMiddleware to verify token
func TokenVerifyMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.Request.Header.Get("Authorization")
		bearerToken := strings.Split(authHeader, " ")

		if len(bearerToken) == 2 {
			authToken := bearerToken[1]

			token, err := jwt.Parse(authToken, func(token *jwt.Token) (i interface{}, err error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("There Was An Error %w", err)
				}
				return []byte(os.Getenv("SECRET")), nil
			})

			if err != nil {
				fmt.Println("There are error with : %w", err.Error())
				utils.HandleError(c, http.StatusUnauthorized, "Ooppss , something when wrong")
				c.Abort()
				return
			}

			if token.Valid {
				fmt.Println("Token Verified")
			} else {
				utils.HandleError(c, http.StatusUnauthorized, "Invalid Token")
				c.Abort()
				return
			}

		} else {
			utils.HandleError(c, http.StatusUnauthorized, "Invalid Token")
			c.Abort()
			return
		}
	}
}
