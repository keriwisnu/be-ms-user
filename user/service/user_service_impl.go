package service

import (
	"errors"

	"gitlab.com/keriwisnu/be-ms-user/model"
	"gitlab.com/keriwisnu/be-ms-user/user"
)

// UserServiceImpl ...
type UserServiceImpl struct {
	userRepo user.UserRepo
}

// CreateUserServiceImpl ...
func CreateUserServiceImpl(userRepo user.UserRepo) user.UserService {
	return &UserServiceImpl{userRepo}
}

// CreateUser ...
func (u *UserServiceImpl) CreateUser(data *model.User) (*model.User, error) {
	if err := data.Validate(); err != nil {
		return nil, err
	}

	user, err := u.userRepo.CreateUser(data)
	if err != nil {
		return nil, err
	}
	return user, nil
}

// GetAll ...
func (u *UserServiceImpl) GetAll() (*[]model.User, error) {
	user, err := u.userRepo.GetAll()
	if err != nil {
		return nil, err
	}
	return user, nil
}

// GetByID ...
func (u *UserServiceImpl) GetByID(id int) (*model.User, error) {
	user, err := u.userRepo.GetByID(id)
	if err != nil {
		return nil, err
	}
	return user, nil
}

// UpdateUser ...
func (u *UserServiceImpl) UpdateUser(id int, data *model.User) (*model.User, error) {
	firstData, err := u.userRepo.GetByID(id)
	if err != nil {
		return nil, errors.New("userID does not exist")
	}

	if data.Email == "" {
		data.Email = firstData.Email
	}
	if data.Username == "" {
		data.Username = firstData.Username
	}
	if data.Password == "" {
		data.Password = firstData.Password
	}
	if data.Role == "" {
		data.Role = firstData.Role
	}

	if err := data.Validate(); err != nil {
		return nil, err
	}

	user, err := u.userRepo.UpdateUser(id, data)
	if err != nil {
		return nil, err
	}

	return user, nil

}

// DeleteUser ...
func (u *UserServiceImpl) DeleteUser(id int) error {
	_, err := u.userRepo.GetByID(id)
	if err != nil {
		return errors.New("userID does not exist")
	}

	err = u.userRepo.DeleteUser(id)
	if err != nil {
		return err
	}

	return nil
}

// CheckEmail ...
func (u *UserServiceImpl) CheckEmail(email string) bool {
	emails := u.userRepo.CheckEmail(email)
	return emails
}

// GetUserByEmail ...
func (u *UserServiceImpl) GetUserByEmail(email string) (*model.User, error) {
	checkEmail := u.userRepo.CheckEmail(email)

	if checkEmail == false {
		return nil, errors.New("Email Not Found")
	}

	users, err := u.userRepo.GetUserByEmail(email)
	if err != nil {
		return nil, errors.New("Oppss, something error")
	}

	return users, nil
}
