package user

import "gitlab.com/keriwisnu/be-ms-user/model"

// UserRepo ...
type UserRepo interface {
	CreateUser(data *model.User) (*model.User, error)
	GetAll() (*[]model.User, error)
	GetByID(id int) (*model.User, error)
	UpdateUser(id int, data *model.User) (*model.User, error)
	DeleteUser(id int) error
	CheckEmail(email string) bool
	GetUserByEmail(email string) (*model.User, error)
}
