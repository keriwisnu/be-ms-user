package config

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/keriwisnu/be-ms-user/model"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres" // import library postgres
)

// Connect ...
func Connect() *gorm.DB {
	conn := os.Getenv("URL")
	db, err := gorm.Open("postgres", conn)
	if err != nil {
		fmt.Println("Error to connect data base ")
		log.Fatal()
	}

	InitTable(db)

	fmt.Println("Success connect to database")

	return db
}

// InitTable ...
func InitTable(db *gorm.DB) {
	db.Debug().AutoMigrate(&model.User{})
}
